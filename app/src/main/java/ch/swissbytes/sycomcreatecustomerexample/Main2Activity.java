package ch.swissbytes.sycomcreatecustomerexample;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import ch.swissbytes.syscomcreatecustomer.Customer;
import ch.swissbytes.syscomcreatecustomer.CustomerData;
import ch.swissbytes.syscomcreatecustomer.DialogEditCustomerNit;


public class Main2Activity extends AppCompatActivity {
    Customer result;
    private final String TAG = this.getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        DialogEditCustomerNit dialog = new DialogEditCustomerNit();
        dialog.show(this, new CustomerData("1", "2"), (customer) -> {
//            customer
            Log.d(TAG, "customer: "+customer.toString());
            return null;
        });
    }
}
