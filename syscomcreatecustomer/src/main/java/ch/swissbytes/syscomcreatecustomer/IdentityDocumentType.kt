package ch.swissbytes.syscomcreatecustomer

import androidx.annotation.StringRes
import java.io.Serializable

enum class IdentityDocumentTypeEnum(taxCode: Long, @StringRes val res: Int) : Serializable {
    NIT(5L, R.string.identity_document_type_nit),
    CI(1L, R.string.identity_document_type_ci),
    CEX(2L, R.string.identity_document_type_cex),
    PAS(3L, R.string.identity_document_type_pas),
    OD(4L, R.string.identity_document_type_od)
}