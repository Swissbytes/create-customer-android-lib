package ch.swissbytes.syscomcreatecustomer

class Customer {
    var id: Long = 0
    var name: String = ""
    lateinit var invoiceName: String
    lateinit var type: String
    lateinit var nit: String
    lateinit var docNumber: String
    lateinit var telf: String
    lateinit var mail: String
    var agencyId: Long = 0
    var businessUnitId: Long = 0
    var identityDocumentType: String? = ""

    override fun toString(): String =
            "id: $id " +
            "name: $name, " +
            "invoiceName: $invoiceName, " +
            "type: $type, " +
            "documentType: $identityDocumentType, " +
            "nit: $nit, " +
            "docNumber: $docNumber," +
            "telf: $telf, " +
            "mail: $mail, " +
            "agencyId: $agencyId"
}

data class CustomerData(var nit: String?, var invoiceName: String?)
