package ch.swissbytes.syscomcreatecustomer

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import kotlinx.android.synthetic.main.content_dialog.*


class DialogEditCustomerNit {

    fun show(context: Context, input: CustomerData, listener: (CustomerData) -> Unit) {
        val customer = CustomerData(input.nit, input.invoiceName)

        val dialog = MaterialDialog(context).show {
            title(R.string.customer_data)
            positiveButton(android.R.string.ok)
            customView(R.layout.change_nit_dialog, scrollable = true)
            noAutoDismiss()
            positiveButton { material ->
                run {
                    val mapValidation = mutableMapOf<EditText, Int>()

                    mapValidation[invoiceName] = R.string.customer_dialog_invoice_name_error_message
                    mapValidation[nit] = R.string.customer_dialog_nit_error_message

                    var isValid = validForm(mapValidation)

                    if (isValid) {
                        customer.invoiceName = invoiceName.text.toString()
                        customer.nit = nit.text.toString()

                        listener.invoke(customer)
                        material.cancel()
                    }
                }
            }
        }

        dialog.getCustomView().apply {
            val invoiceNameView = findViewById<EditText>(R.id.invoiceName)
            val nitView = findViewById<EditText>(R.id.nit)
            nitView.preventSpace()

            invoiceNameView.setText(customer.invoiceName)
            nitView.setText(customer.nit)
        }

    }

    private fun EditText.preventSpace(){
        addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s?.contains(" ") == true){
                    setText(s.trim())
                }
            }
        })
    }


    private fun EditText?.isValid(@StringRes errorRes: Int?): Boolean {
        if (this == null) return false
        return if (text.toString().isBlank()) {
            errorRes.let {
                error = context.getString(it!!)
                requestFocus()
                showKeyboard()
            }
            false
        } else true
    }

    private fun View.showKeyboard() {
        val inputMethodManager = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun validForm(map: Map<EditText, Int>): Boolean {
        for ((key, value) in map) {
            if (!key.isValid(value)) {
                return false
            }
        }
        return true
    }
}
