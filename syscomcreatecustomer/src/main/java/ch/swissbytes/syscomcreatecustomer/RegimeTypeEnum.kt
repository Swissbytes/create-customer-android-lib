package ch.swissbytes.syscomcreatecustomer

import androidx.annotation.StringRes
import java.io.Serializable

enum class RegimeTypeEnum (@StringRes val res: Int) : Serializable {

    NATURAL_PERSON(R.string.regime_type_natural),
    GENERAL(R.string.regime_type_general)
}
