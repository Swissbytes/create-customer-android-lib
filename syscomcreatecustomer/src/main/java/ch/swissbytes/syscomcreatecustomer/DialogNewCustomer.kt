package ch.swissbytes.syscomcreatecustomer

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import kotlinx.android.synthetic.main.content_dialog.*


class DialogNewCustomer {

    fun show(context: Context, listener: (Customer) -> Unit) {
        val customer = Customer()

        val dialog = MaterialDialog(context).show {
            customView(R.layout.content_dialog, scrollable = true)
            noAutoDismiss()
            positiveButton { material ->
                run {
                    customer.name = name.text.toString()
                    customer.invoiceName = invoiceName.text.toString()
                    customer.nit = nit.text.toString()
                    customer.docNumber = docNumber.text.toString()
                    customer.telf = telf.text.toString()
                    customer.mail = mail.text.toString()

                    //typeGeneral mean juridica
                    val typeGeneral = rb_general!!.isChecked

                    customer.type = if (typeGeneral) RegimeTypeEnum.GENERAL.name
                                    else RegimeTypeEnum.NATURAL_PERSON.name

                    val mapValidation = mutableMapOf<EditText, Int>()

                    if (!typeGeneral) {
                        //Persona natural
                        customer.invoiceName = customer.name
                        mapValidation[name] = R.string.customer_dialog_name_error_message
                        mapValidation[nit] = R.string.customer_dialog_nit_error_message
                    } else {
                        //Persona juridica nit is required
                        customer.name = customer.invoiceName

                        mapValidation[invoiceName] = R.string.customer_dialog_invoice_name_error_message
                        mapValidation[docNumber] = R.string.customer_dialog_carnet_error_message
                        mapValidation[nit] = R.string.customer_dialog_nit_error_message
                    }

                    var isValid = validForm(mapValidation)

                    if (isValid){
                        if (!mail.text.isBlank()){
                            isValid = Patterns.EMAIL_ADDRESS.matcher(mail.text.toString()).matches()
                            if (!isValid){
                                mail.error = getContext().getString(R.string.customer_dialog_mail_error_invalid_message)
                            }
                        }
                    }

                    if (customer.identityDocumentType == null) {
                        etDocType.error = getContext().getString(R.string.identity_document_type_required)
                        isValid = false
                    }

                    if (isValid) {
                        listener.invoke(customer)
                        material.cancel()
                    }
                }
            }
            positiveButton(android.R.string.ok)
            title(R.string.title_dialog_new_customer)
        }

        dialog.getCustomView().apply {
            val rg = findViewById<RadioGroup>(R.id.RGroup)
            val nameView = findViewById<View>(R.id.name)
            val invoiceNameView = findViewById<View>(R.id.invoiceName)
            val documentType = findViewById<EditText>(R.id.etDocType)
            val nitView = findViewById<EditText>(R.id.nit)
            val docNumber = findViewById<EditText>(R.id.docNumber)
            val mail = findViewById<EditText>(R.id.mail)

            mail.preventSpace()
            nitView.preventSpace()
            docNumber.preventSpace()

            documentType.setOnClickListener {
                showDocumentTypeDialog(context, customer, (it as EditText))
            }

//            val identityDocumentTypeAdapter = getIdentityDocumentTypeAdapter(context)
//            identityDocumentTypeSpinner?.adapter = identityDocumentTypeAdapter
//            setItemSelectedListener(
//                identityDocumentTypeSpinner,
//                identityDocumentTypeAdapter,
//                customer
//            )

            rg.setOnCheckedChangeListener { _, checkedId ->
                val isGeneral = checkedId == R.id.rb_general //Juridica

                nameView.visibility = if (isGeneral) View.GONE else View.VISIBLE
                invoiceNameView.visibility = if (isGeneral) View.VISIBLE else View.GONE
//                nitView.visibility = if (isGeneral) View.GONE else View.VISIBLE
            }


            //Nit should ed should be filled by docNumber if empty
            docNumber.setOnFocusChangeListener { v, hasFocus ->
                if (!hasFocus){ // When the docNumber lost the focus
                    if (nitView.text.isEmpty() || nitView.text.isBlank()){
                        val nit = docNumber.text.toString().onlyNumbers()
                        nitView.setText(nit)
                        postDelayed({ nitView.setSelection(nitView.text.length) }, 200)
                    }

                }
            }
        }

    }

    //TODO move to base
    private fun String.onlyNumbers(): String = this.toCharArray()
        .filter { "0123456789".contains(it, true) }.joinToString("")

    private fun EditText.preventSpace(){
        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s?.contains(" ") == true) {
                    setText(s.trim())
                }
            }
        })
    }


    private fun EditText?.isValid(@StringRes errorRes: Int?): Boolean {
        if (this == null) return false
        return if (text.toString().isBlank()) {
            errorRes.let {
                error = context.getString(it!!)
                requestFocus()
                showKeyboard()
            }
            false
        } else true
    }

    private fun View.showKeyboard() {
        val inputMethodManager = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun validForm(map: Map<EditText, Int>): Boolean {
        for ((key, value) in map) {
            if (!key.isValid(value)) {
                return false
            }
        }
        return true
    }

//    private fun getIdentityDocumentTypeAdapter(context: Context) = ArrayAdapter<EnumTextItem<IdentityDocumentTypeEnum>>(
//        context,
//        android.R.layout.simple_spinner_item, getListOfIdentityDocumentTypes(context)
//    )
//            .also { adapter ->
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//    }

    private fun getListOfIdentityDocumentTypes(context: Context) = listOf(
        EnumTextItem(
            IdentityDocumentTypeEnum.NIT,
            context.getString(IdentityDocumentTypeEnum.NIT.res)
        ),
        EnumTextItem(
            IdentityDocumentTypeEnum.CI,
            context.getString(IdentityDocumentTypeEnum.CI.res)
        ),
        EnumTextItem(
            IdentityDocumentTypeEnum.CEX,
            context.getString(IdentityDocumentTypeEnum.CEX.res)
        ),
        EnumTextItem(
            IdentityDocumentTypeEnum.OD,
            context.getString(IdentityDocumentTypeEnum.OD.res)
        ),
        EnumTextItem(
            IdentityDocumentTypeEnum.PAS,
            context.getString(IdentityDocumentTypeEnum.PAS.res)
        )
    )

    private fun showDocumentTypeDialog(context: Context, customer: Customer, editText: EditText) {
        val documentTypes = IdentityDocumentTypeEnum.values()
        val documentTypeNames = documentTypes.map { context.getString(it.res) }
        MaterialDialog(context).show { 
            listItemsSingleChoice(items = documentTypeNames) { _ , index, text ->
                customer.identityDocumentType = documentTypes[index].name
                editText.setText(text)
            }
        }
    }
}

class EnumTextItem<T>(val id: T?, val text: String) {
    override fun toString(): String {
        return text
    }
}


